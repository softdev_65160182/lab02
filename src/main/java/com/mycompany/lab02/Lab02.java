/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;
import java.util.Scanner;

/**
 *
 * @author PT
 */
public class Lab02 {
    static char [][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}
    };
    static char currentPlayer ='X';
    static int row, col;
    static String answer;
    static void printWelcome(){
        System.out.println("Welcome to OX");
    }
    
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
        System.out.print("Please input row col :");
        row = sc.nextInt();
        col = sc.nextInt();
        if(table[row - 1][col - 1] == '-'){
           table[row - 1][col - 1]= currentPlayer;
           break;
        } 
      }
    }
    static void switchPlayer(){
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }
    static boolean isWin(){
        if(checkCol()==true){
            return true;
        }
        if(checkRow()==true){
                return true;
        }
        if(checkxR()){
                return true;
        }
        if(checkxL()){
                return true;
        }
        return false;
}
    
    static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[row-1][i]!= currentPlayer){
                return false;
            }
        }
         return true;
    }
    static boolean checkCol(){
        for(int i=0; i<3 ;i++){
            if(table[i][col-1] != currentPlayer){
                return false;
            }
        }
        return true;
    }
    static boolean checkxR(){
            if(table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer){
                return true;
            }
        
        return false;
     }
    static boolean checkxL(){
            if(table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer){
                return true;
            }
        
        return false;
     }
    
    static void printWin(){
        System.out.println(currentPlayer+" Win");
    }
    static void printDraw(){
        System.out.println(" Draw");
    }
    static boolean isDraw(){
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
      }
    public static void main(String[] args) {
        printWelcome();
        while (true){
        printTable();
        printTurn();
        inputRowCol();
        if(isWin()){
            printTable();
            printWin();
            break;
        }
        if(isDraw()){
            printTable();
            printDraw();
            break;
        }
        switchPlayer();
       } 
    }
}
